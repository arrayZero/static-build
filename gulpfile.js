var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync  = require('browser-sync').create(),
    runSequence  = require('run-sequence'),
    cleanCSS     = require('gulp-clean-css'),
    concat       = require('gulp-concat'),
    pug          = require('gulp-pug'),
    uglify       = require('gulp-uglify'),
    sass         = require('gulp-sass'),
    imagemin     = require('gulp-imagemin'),
    sourcemaps   = require('gulp-sourcemaps'),
    path         = require('path'),
    rename       = require('gulp-rename'),
    clean        = require('gulp-clean'),
    menu         = require('./assets/config/nav.json');

gulp.task('default', function () {
    runSequence('clean', 'copyfonts', ['sass', 'templates', 'compress', 'images', 'browser-sync', 'watch']);
});


gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./public_html"
        }
    });
});

gulp.task('clean', function() {
    return gulp.src('public_html/', {read: false})
        .pipe(clean({force: true}));
});

gulp.task('copyfonts', function() {
   gulp.src(['./bower_components/font-awesome/fonts/**/*.{ttf,woff,eof,svg}',
            './assets/fonts/*.{ttf,woff,eof,svg}'])
   .pipe(gulp.dest('./public_html/fonts'));
});

gulp.task('sass', function () {
    return gulp.src('./assets/sass/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
         }))
        .pipe(sourcemaps.write())
        .pipe(rename({basename: "styles"}))
        .pipe(gulp.dest('public_html/css/'))
        .pipe(browserSync.stream());
});

gulp.task('compress', function () {
    return gulp.src(['./bower_components/jquery/dist/jquery.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/alert.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/button.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/carousel.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/popover.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/tab.js',
                    './bower_components/bootstrap-sass/assets/javascripts/bootstrap/affix.js',
                     './assets/js/*.js'])
        .pipe(concat('scripts_min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public_html/js/'))
        .pipe(browserSync.stream());
});


gulp.task('templates', function () {
    //set up a menu based on JSON
    var locals = {'menu': menu};
    gulp.src('./templates/*.pug')
        .pipe(pug({locals: locals,pretty: true}))
        .pipe(gulp.dest('public_html/'))
        .pipe(browserSync.stream());
});


gulp.task('images', function() {
  return gulp.src('./assets/images/*')
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
    }))
    .pipe(gulp.dest('public_html/images'))
});



// gulp.task('copy', function () {
//     gulp.src('public_html/**')
//         .pipe(gulp.dest('../static/public_html/protected/csl-behring/berinert-iva/'));
// });

gulp.task('watch', function () {
    gulp.watch('assets/sass/**/*.scss', ['sass']);
    gulp.watch('assets/js/*.js', ['compress']);
    gulp.watch('assets/config/*.json', ['templates']);
    gulp.watch('templates/**/*.pug', ['templates']);
    gulp.watch('assets/images/*', ['images']);
});
