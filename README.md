# Tempalting system for fingerpaint static sites & pages #
The plan is for the is to evolve over time, so please download and make changes.

### What we are trying to accomplish? ###

* Quickly build responsive html sites & pages.

### How do I get set up? ###

1. Pull the repo... 
2. Run 'npm install'
3. Run 'bower install'


### Contribution guidelines ###

* Please review any possible updates with the rest of the team before pushing to master.